/* File Name: log.h */
/* Author: zuoyanyouwu */
#ifndef _DEBUG_H_
#define _DEBUG_H_

#define DEBUG() do {fprintf(stderr, "%s(%d):{%s}\n", __FILE__, __LINE__, __func__);} while (0)

#endif // _DEBUG_H_