#include <sys/syslog.h>
#include <sys/param.h>
#include <sys/stat.h>

#ifndef _INIT_DAEMON_H_
#define _INIT_DAEMON_H_

void init_daemon(const char *pname, int facility);

#endif  // end of _INIT_DEAMON_H_
